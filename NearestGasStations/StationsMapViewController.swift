//
//  StationsMapViewController.swift
//  NearestGasStations
//
//  Created by Hossam on 4/21/17.
//  Copyright © 2017 Hossam. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit


class StationsMapViewController: UIViewController,LocationServiceDelegate, MKMapViewDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    let stationsExtractor = StationsExtractor()
    var arrGasStations = [Station]();
    var trasnportationType : MKDirectionsTransportType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.isConnectedToNetwork(){
            mapView.setUserTrackingMode(MKUserTrackingMode.none, animated: true)
            
            mapView.delegate = self
            
            LocationService.sharedInstance.delegate = self
            LocationService.sharedInstance.startUpdatingLocation()
            
        }
        else{
            
            let noConnectionAlertController = UIAlertController(title: "Error", message: "there is no internet connection", preferredStyle: UIAlertControllerStyle.alert)
            noConnectionAlertController.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(noConnectionAlertController, animated: true, completion: nil)
            
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        mapView.frame = self.view.bounds
        mapView.autoresizingMask = self.view.autoresizingMask
    }
    
    
    
    func displayGasStationsInMap() {
        // remove all previousely added annotations before adding new annotations
        mapView.removeAnnotations(mapView.annotations)
        
        
        // add the Gas Stations in map as annotations
        for gasStation in arrGasStations {
            let annotation = MKPointAnnotation()
            annotation.title = gasStation.name
            annotation.coordinate = CLLocationCoordinate2D(latitude: gasStation.latitude!, longitude: gasStation.longitude!)
            mapView.addAnnotation(annotation)
        }
    }
    
    
    
    
    func containSameElements(arrStationsOld : [Station], arrStationsNew : [Station]) -> Bool {
        guard arrStationsOld.count == arrStationsNew.count else {
            return false // not same count mean not same arrays
        }
        
        return arrStationsOld.last?.name == arrStationsNew.last?.name
    }
    
    // MARK: - CLLocation delegate methods
    func tracingLocation(_ currentLocation: CLLocation){
        
        print("current position: \(currentLocation.coordinate.longitude) , \(currentLocation.coordinate.latitude)")
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(currentLocation.coordinate,
                                                                  1000 * 2.0, 1000 * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        stationsExtractor.location = currentLocation
        stationsExtractor.loadGasStationsList(  radius: 1000, callback: {
            arrStations in
            if arrStations != nil{
                
                if !self.containSameElements(arrStationsOld: self.arrGasStations, arrStationsNew: arrStations!){
                    self.arrGasStations = arrStations!
                    
                    self.displayGasStationsInMap()
                    
                    self.setTransportationTypeAccordingSpeed(currentSpeed: currentLocation.speed)
                }
                
            }
            else{
                print("something wrong happen")
            }
            
        })
        //
    }
    func tracingLocationDidFailWithError(_ error: NSError) {
        print("didFailWithError: \(error.description)")
        
        let locationErrorAlertController = UIAlertController(title: "Error", message: "Failed to Get Your Location", preferredStyle: UIAlertControllerStyle.alert)
        locationErrorAlertController.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(locationErrorAlertController, animated: true, completion: nil)
        
        
        
    }
    
    // MARK: - Direction detection to a gas station
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        if let annotationTitle = view.annotation?.title
        {
            print("User tapped on annotation with title: \(annotationTitle!)")
            displayRoute(annotation: view.annotation! , transportType: MKDirectionsTransportType.automobile)
        }
    }
    func displayRoute(annotation : MKAnnotation , transportType : MKDirectionsTransportType){
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: (LocationService.sharedInstance.currentLocation?.coordinate)!, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: annotation.coordinate, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = transportType
        
        let directions = MKDirections(request: request)
        self.mapView.removeOverlays(mapView.overlays)
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            
            var shortestRoute : MKRoute?
            for route in unwrappedResponse.routes {
                if shortestRoute == nil{
                    shortestRoute = route
                }
                else if route.distance < (shortestRoute?.distance)!{
                    shortestRoute = route
                }
                
            }
            self.mapView.add((shortestRoute?.polyline)!)
            self.mapView.setVisibleMapRect((shortestRoute?.polyline.boundingMapRect)!, animated: true)
        }
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }
    func setTransportationTypeAccordingSpeed(currentSpeed : Double){
        if currentSpeed < 10 {
            trasnportationType = MKDirectionsTransportType.walking
        }
        else if currentSpeed < 50{
            trasnportationType = MKDirectionsTransportType.transit
            
        }
        else{
            trasnportationType = MKDirectionsTransportType.automobile
        }
        
        if trasnportationType == nil {
            trasnportationType = MKDirectionsTransportType.automobile
        }
    }
    
    
}

