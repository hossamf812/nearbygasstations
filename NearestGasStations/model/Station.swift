//
//  Station.swift
//  WorkStations
//
//  Created by Hossam on 4/21/17.
//  Copyright © 2017 Hossam. All rights reserved.
//

import UIKit
import GooglePlaces

class Station: NSObject {
     var name : String?
     var longitude : Double?
     var latitude : Double?
     var distance : Double?
    init(name : String ,longitude : Double ,latitude : Double) {
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
        
    }
    
    /// assign to the distance variable which is the distance in meters between the station Location and the user location
    ///
    /// - Parameter location: it represents the user location
    func setDistanceToLocation(location : CLLocation)  {
        distance = location.distance(from: CLLocation(latitude: latitude!, longitude: longitude!))
        
        
    }
}
