//
//  StationsExtractor.swift
//  WorkStations
//
//  Created by Hossam on 4/21/17.
//  Copyright © 2017 Hossam. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces

class StationsExtractor: NSObject {
    
    var location : CLLocation!
    
    func loadGasStationsList( radius : Double,  callback:@escaping (_ arrGasStations: [Station]?) -> ()) {
        let currentPositionString = "\(location!.coordinate.latitude),\(location!.coordinate.longitude)"
        
        var urlString = GoogleMapConfiguration.googlePlacesNearbySearchURL
        urlString.append("location=\(currentPositionString)&")
        urlString.append("radius=\(radius)&")
        urlString.append("type=gas_station&")
        urlString.append("key=\(GoogleMapConfiguration.apiKEY)")
        print(urlString)
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(urlString, method: .get, parameters: nil , encoding: JSONEncoding.default).responseJSON { response in
                
                
                print("Resp=\(response.result)")
                
                switch response.result {
                case .success:
                    if let jsonResponse = response.result.value {
                        
                        let dicStations = jsonResponse as! NSDictionary
                        let arrData = self.parseStationsDataResult(dicStations: dicStations)
                        if arrData == nil{
                            callback(nil)
                        }
                        else{
                            callback(arrData)
                        }
                        
                    }
                    break
                case .failure(let error):
                    print("failure>> \(error)")
                    
                    // google server failed to get the result
                    callback(nil)
                    
                }
            }
        } else {
            // there is no internet connection
            callback(nil)
            
        }
    }
    
    func parseStationsDataResult(dicStations: NSDictionary?) -> [Station]!{
        var arrStations : [Station] = Array()
        
        
        
        if let arrResult = dicStations?["results"] as? [[String: Any]] {
            for stationItem in arrResult{
                let stationName = "\(stationItem["name"]!)"
                let geometry = stationItem["geometry"] as! NSDictionary
                let locationDic = geometry["location"] as! NSDictionary
                let latitude =  (Double)(locationDic["lat"]! as! NSNumber)
                let longitude = (Double)(locationDic["lng"]! as! NSNumber)
                
                let station = Station(name: stationName, longitude: longitude, latitude: latitude)
                station.setDistanceToLocation(location: self.location!)
                arrStations.append(station)
                print("station name is :\(stationName) and lat : \(latitude) and lng : \(longitude)")
            }
        }
        else{
            return nil
        }
        
        return arrStations
    }
    
    
}
