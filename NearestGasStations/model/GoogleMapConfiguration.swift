//
//  GoogleMapConfiguration.swift
//  NearestGasStations
//
//  Created by Hossam Fathy on 4/21/17.
//  Copyright © 2017 Hossam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class GoogleMapConfiguration: NSObject {
    static let apiKEY = "AIzaSyA2V-vnuTWoMYrMos6DqXuXoFvS2eLAi1M"
    static let googlePlacesNearbySearchURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    override init() {
        // the api key is unrestricted
        GMSServices.provideAPIKey(GoogleMapConfiguration.apiKEY)
        GMSPlacesClient.provideAPIKey(GoogleMapConfiguration.apiKEY)
        
    }
    
    
}
