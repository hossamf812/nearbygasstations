//
//  StationsListViewController.swift
//  NearestGasStations
//
//  Created by Hossam on 4/21/17.
//  Copyright © 2017 Hossam. All rights reserved.
//

import UIKit
import CoreLocation
class StationsListViewController: UIViewController,UITableViewDataSource, LocationServiceDelegate {
    @IBOutlet weak var gasStationsTableView: UITableView!
    let stationsExtractor = StationsExtractor()
    var arrGasStations = [Station]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork(){
            LocationService.sharedInstance.delegate = self
            LocationService.sharedInstance.startUpdatingLocation()
            
        }
        else{
            let noConnectionAlertController = UIAlertController(title: "Error", message: "there is no internet connection", preferredStyle: UIAlertControllerStyle.alert)
            noConnectionAlertController.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(noConnectionAlertController, animated: true, completion: nil)
            
            
        }
    }
    
    
    // MARK: - TableView delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGasStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let gasStation = arrGasStations[indexPath.row]
        
        // dequeue a cell for the given indexPath
        let cell = tableView.dequeueReusableCell(withIdentifier: "stationCell", for: indexPath) as! StationTableViewCell
        
        
        // set the cell's text with the new string formatting
        cell.lblTitle.text = gasStation.name
        cell.lblSubTitle.text = "\(Int(round(gasStation.distance! ))) Meter"
        return cell
    }
    // MARK: - CLLocation delegate methods
    func tracingLocation(_ currentLocation: CLLocation){
        
        print("current position: \(currentLocation.coordinate.longitude) , \(currentLocation.coordinate.latitude)")
        
        
        
        stationsExtractor.location = currentLocation
        stationsExtractor.loadGasStationsList(  radius: 1000, callback: {
            arrStations in
            if arrStations != nil{
                
                self.arrGasStations = arrStations!
                self.arrGasStations.sort { CGFloat( $0.distance!) < CGFloat($1.distance!) }
                
                self.gasStationsTableView.reloadData()
                
            }
            else{
                print("something wrong happen")
            }
            
        })
        
    }
    func tracingLocationDidFailWithError(_ error: NSError) {
        print("didFailWithError: \(error.description)")
        
        let locationErrorAlertController = UIAlertController(title: "Error", message: "Failed to Get Your Location", preferredStyle: UIAlertControllerStyle.alert)
        locationErrorAlertController.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(locationErrorAlertController, animated: true, completion: nil)
        
        
    }
}

